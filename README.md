# Transformer une classe de collège en calculateur parallèle pour résoudre l'équation de la chaleur en deux dimensions

Une activité mise en place dans le cadre de la [semaine des mathématiques 2021](https://www.education.gouv.fr/la-semaine-des-mathematiques-7241) dans une [classe de 4ème](http://www.col-brant-eschau.ac-strasbourg.fr/spip.php?article432) du collègue d'Eschau (Bas-Rhin). 

## Installer les dépendances

```bash
pip install -r requirements.txt
```

## Exécuter localement le notebook

```bash
jupyter-notebook chaleur2d.ipynb
```

## Exécuter en ligne avec Mybinder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fboileau%2Fchaleur2d/master?filepath=chaleur2d.ipynb)